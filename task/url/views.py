from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse

from url.models import Url


@login_required(login_url='/admin/login/')
def home_url(request):
    urls = Url.objects.all()
    return render(request, 'urls.html', context={'url_list': urls})


def ajax_test(request):
    if request.is_ajax() and request.method == "GET":
        id = request.GET.get("id")
        db_urls = Url.objects.filter(id=id)
        db_url = db_urls.first()
        db_url = db_url.save()
        return JsonResponse(id, safe=False)
    return JsonResponse({"status": 'false' }, status=500)
