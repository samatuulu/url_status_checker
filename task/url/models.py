from django.db import models
import requests
from requests.exceptions import ConnectionError, MissingSchema

class Url(models.Model):
    url_name = models.CharField(max_length=100, help_text='with http/s')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)
    status = models.IntegerField(null=True, blank=True, help_text='please skip this filed')

    def save(self, *args, **kwargs):
        try:
            status_r = requests.get(self.url_name, timeout=90)
            self.status = status_r.status_code
            super(Url, self).save(*args, **kwargs)
        except MissingSchema:
            print('Url is not complete')
        except ConnectionError as e:
            print('No response')

    def __str__(self):
        url = self.url_name
        return url
