from django import urls
from django.contrib import admin

from url.models import Url

admin.site.register(Url)
